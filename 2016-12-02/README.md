# prepareDelivery.sh

Ein Shellskript zum Vorbereiten einer Dokumentenauslieferung.

Zur Auslieferung der Software für den Anwendungskonnektor gehört auch die Übergabe der Dokumentation an den Evaluator der Software. Die Dokumente müssen einzeln verschlüsselt werden, zusätzlich muss zum Integritätsschutz eine Prüfsumme für jedes Dokument berechnet werden (SHA256). Die Prüfsumme muss ebenfalls verschlüsselt werden. Somit müssen pro Datei Operationen durchgeführt werden.

Das vorliegende Skript prepareDelivery.sh übernimmt diese Aufgabe. Es erhält beim Aufruf den Namen des Verzeichnisses, in dem die Dokumente zur Auslieferung stehen:

    $ ./prepareDelivery.sh <dirname>

Das Verzeichnis "./Final" enthält eine beispielhafte Dokumentenstruktur.

gpg unter Cygwin/Windows versteht die Verweise auf die Schlüsselringdateien nicht korrekt. Daher liegt hier auch ein Vagrant-File, um eine Linux VM starten zu können.

Praesentation/slides.key.pdf zeigt die Entwicklung des Skripts als Beispiel für das iterative Vorgehen bei der Entwicklung von Shell Skripten.
