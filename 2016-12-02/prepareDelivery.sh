#!/bin/bash 

documentdir="$1"

if [ -z "$documentdir" ]; then
   echo "Usage: $0 documentDirectory"
   exit 1
fi

export keyringConfig="--no-default-keyring --keyring ./keys.gpg"

recipientList() {
   gpg $keyringConfig --list-keys --with-colons --fast-list-mode | awk -F: '/^pub/{printf "-r %s ", $5}'
}

mygpg() {
   gpg $keyringConfig --trust-model always --encrypt $(recipientList) "$@"
}

find "$documentdir" -type f -name "*gz" -o -name "*zip" -o -name "*diff" | while read i; do 
   fileNoSuffix=${i%.*}
   checksumFile="${fileNoSuffix}_sha256sum.txt"
   sha256sum "$i" >"${checksumFile}"
   mygpg "$i"
   mygpg "$checksumFile"
done
   

